'use strict';

// Nombre total de soutiens requis (4.7 Million)
const requiredTotal = 4717396;

// calcul théorique pour estimer les 10 % du corps électoral nécessaire
// soit ~ 4 717 396 personnes / 274 jours (du 13 juin 2019 au 13 mars 2020)
// moyenne journalière à tenir d'environ 17 217 soutiens...
const perDay_required = 17217;

// Date d'aujourd'hui
var today = moment().startOf("days")

const dateStart = moment("2019-06-13")
const dateEnd = moment("2020-03-12");
var currentDataTs;
var datesList;

$(() => {
    $('#header').load('navigation.html');
    onStart();
    setIntro();
});

$.get('./archive/history.dat?nocache', function (result) {
    $.csv.toArrays(result, {
        "separator": ";"
    }, function (err, rawData) {
        $.get('./archive/latest.count.json?nocache', function (result) {
            // permet de combler les "jours sans" (le cas échéant)
            var latestDate = moment(+result["timestamp"] * 1000).format("YYYY-MM-DD");
            var historyDate = moment(+rawData[rawData.length - 1][0] * 1000).format("YYYY-MM-DD");
            var currentDate = today.format("YYYY-MM-DD");

            var latestCount = result["total"];
            var latestRowCount = +rawData[rawData.length - 1][1];

            var dateSameOrBefore = (moment(historyDate).isSameOrBefore(moment(latestDate)));
            var dateAfter = (moment(historyDate).isAfter(moment(latestDate)));
            var checkCount = (latestCount === latestRowCount);

            var firstDate = moment(+rawData[0][0] * 1000).format("YYYY-MM-DD");
            var endDate;

            if (dateSameOrBefore && checkCount) {
                endDate = +rawData[rawData.length - 1][0];
            } else if (dateAfter && checkCount || dateAfter && !checkCount) {
                rawData.splice(-1, 1);
                endDate = +result["timestamp"];
            } else {
                document.getElementById('update-test').innerHTML = '<small class="animated flash infinite"> (actualisation en cours...)</small>';
                rawData.push([Math.floor(Date.now() / 1000), result["total"]]);
                endDate = +result["timestamp"];
            }
            // console.log(moment(endDate* 1000).format("YYYY-MM-DD"));
            var difference = moment(endDate * 1000).diff(firstDate, 'days');
            // var difference = today.diff(firstDate, 'days');

            var testDates = {};
            rawData.forEach((item, i) => {
                testDates[moment(+item[0] * 1000).format("YYYY-MM-DD")] = +item[1];
            })

            for (var i = 0; i <= difference; i++) {
                var date = moment(firstDate).add(i, "day").format("YYYY-MM-DD");
                var dateSameOrBefore = moment(date).subtract(1, "day").format("YYYY-MM-DD");
                if (testDates[date] === undefined)
                    testDates[date] = testDates[dateSameOrBefore];
            }
            var data = [];
            for (var date in testDates) {
                data.push([date, +testDates[date]]);
            }
            data = data.sort((a, b) => (a[0].valueOf() > b[0].valueOf()) ? 1 : -1);

            // Lissage sur les 5 premiers jours des 26 157 soutiens enregistré en une seule fois à la date du 18/06/2019
            var joursDecalage = [
                ["2019-06-13", 21026],
                ["2019-06-14", 42452],
                ["2019-06-15", 63078],
                ["2019-06-16", 84104],
                ["2019-06-17", 105130]
            ];
            data = joursDecalage.concat(data);
            var config = generateConfig(data, endDate);

            if (err) {
                console.log(err);
                return;
            }

            drawGauge(config);
            drawChart(config);

            $('select#selectCharts').prop('selectedIndex', getUrlParameter('g'));
            $('select#selectCharts').trigger('change');
            $("#mainloader").hide();
        })
    })
})

var dataSerie_officiel;
$.getJSON("./chiffres_cc.json", function (data) {
    dataSerie_officiel = data;
})

function generateConfig(data, latestTimestamp) {
    const lastUpdate = latestTimestamp * 1000;
    const latestCount = data[data.length - 1][1];

    var updateCheck = (moment(lastUpdate).format("YYYY-MM-DD") !== today.format("YYYY-MM-DD"));

    const days_complete = dateEnd.diff(dateStart, 'days') + 1;
    const days_done = (updateCheck) ? today.diff(dateStart, 'days') : today.diff(dateStart, 'days') + 1;
    const days_remaining = days_complete - days_done; //dateEnd.diff(today, 'days') + 1; //

    var chartData = [];
    var cumul = 0,
        days = 1,
        lastMinimale,
        lastMoyenne;

    for (var i = 0, lgi = days_done; i < lgi; i++) {
        var dateStr = moment('2019-06-13').add(i, 'days').format("YYYY-MM-DD"),
            visits = 0,
            difference,
            moyenne_mobile,
            moyenne_minimale;

        cumul += perDay_required;
        data.forEach((arr, i) => {
            if (dateStr === arr[0])
                visits = +arr[1];

            difference = visits - cumul;
            moyenne_mobile = Math.round(visits / days);
            moyenne_minimale = getMoyMinimale(moyenne_mobile, difference, days_remaining)
            if (i === data.length - 1) {
                lastMinimale = moyenne_minimale;
                lastMoyenne = moyenne_mobile;
            }
        });

        chartData.push({
            date: dateStr,
            visits: visits,
            cumul_theorique: cumul,
            difference: difference,
            moyenne_mobile: moyenne_mobile,
            moyenne_minimale: moyenne_minimale
        });

        // visits === 0 n'est pas une condition à ajouter ici car il peut y avoir une ou plusierus journées sans aucun soutien... ?
        chartData[i]['evolution'] = (i > 0 && chartData[i]["visits"] > 0) ? chartData[i]["visits"] - chartData[i - 1]["visits"] : chartData[i]["visits"];
        days++;
        if (dateStr === "2020-03-16")
            break;
    }


    // calculs des différentes moyennes
    var average_last30Days = chartData.slice(chartData.length - 30, chartData.length)
        .map(obj => obj["evolution"]).reduce((a, b) => a += b);
    average_last30Days = Math.round(average_last30Days / 30);

    var average_last14Days = chartData.slice(chartData.length - 14, chartData.length)
        .map(obj => obj["evolution"]).reduce((a, b) => a += b);
    average_last14Days = Math.ceil(average_last14Days / 14);

    var average_last7Days = chartData.slice(chartData.length - 7, chartData.length)
        .map(obj => obj["evolution"]).reduce((a, b) => a += b);
    average_last7Days = Math.round(average_last7Days / 7);

    var averagePerDay_sinceStart = ((latestCount / days_done) >= lastMinimale) ?
        '<span>' + Math.round(latestCount / days_done).toLocaleString('fr') + '</span>' :
        '<span style="color: red" class="animated flash infinite">' + Math.round(latestCount / days_done).toLocaleString('fr') + '</span>';

    // calcul pour afficher les s tatitisques des 7 derniers jours
    var evolution_semaine = chartData.slice(chartData.length - 7, chartData.length)
        .map(obj => '<div class="text-center">' + moment(obj["date"]).format("DD/MM/YYYY") +
            '&emsp;:&emsp;<b>' + obj["evolution"].toLocaleString('fr') + '</b></div>');

    var lastUpdate_msg = (moment(lastUpdate).isValid()) ?
        'dernière mise à jour le ' + moment(lastUpdate).format('LLLL') :
        'date de mise à jour non définie';

    // statisques pour modal
    var contentStats = '<dl class="row">';
    contentStats += '<dt class="col-sm-3 text-right"><b>' +
        days_done + '</b></dt><dd class="col-sm-9">jours écoulés depuis le 13/06/2019 inclus</dd>';
    contentStats += '<dt class="col-sm-3 border-top"></dt><dd class="col-sm-9 border-top"></dd>';
    contentStats += '<dt class="col-sm-3 text-right"><b>' + cumul.toLocaleString('fr') +
        '</b></dt><dd class="col-sm-9">minimum requis total</dd>';
    contentStats += '<dt class="col-sm-3 text-right"><b>' + latestCount.toLocaleString('fr') +
        '</b></dt><dd class="col-sm-9">soutiens publiés</dd>';

    if (cumul > latestCount)
        contentStats += '<dt class="col-sm-3 text-right"><span style="color: red" class="animated flash infinite"><b>' +
        (cumul - latestCount).toLocaleString('fr') + '</b></span></dt><dd class="col-sm-9">soutiens manquants</dd>';

    contentStats += '<dt class="col-sm-3 border-top"></dt><dd class="col-sm-9 border-top"></dd>';
    contentStats += '<dt class="col-sm-3 text-right"><b>' + perDay_required.toLocaleString('fr') +
        '</b></dt><dd class="col-sm-9">minimum de soutiens requis par jour</dd>';
    contentStats += '<dt class="col-sm-3 text-right"><b>' + averagePerDay_sinceStart +
        '</b></dt><dd class="col-sm-9">moyenne journali&egrave;re depuis le 13/06/2019</dd>';
    contentStats += '<dt class="col-sm-3 text-right"><b>' + average_last30Days.toLocaleString('fr') +
        '</b></dt><dd class="col-sm-9">moyenne journali&egrave;re des 30 derniers jours</dd>';
    contentStats += '<dt class="col-sm-3 text-right"><b>' + average_last14Days.toLocaleString('fr') +
        '</b></dt><dd class="col-sm-9">moyenne journali&egrave;re des 14 derniers jours</dd>';
    contentStats += '<dt class="col-sm-3 text-right"><b>' + average_last7Days.toLocaleString('fr') +
        '</b></dt><dd class="col-sm-9">moyenne journali&egrave;re des 7 derniers jours</dd>';
    contentStats += '<dt class="col-sm-3 border-top"></dt><dd class="col-sm-9 border-top"></dd>';
    contentStats += '</dl>';
    contentStats += '<div>&Eacute;volution sur les 7 derniers jours</div>';
    contentStats += evolution_semaine.join('\n');

    $('#statsModalLabel').text('Statistiques au ' + moment(lastUpdate).format("DD/MM/YYYY"))
    $('#statsDiv').html(contentStats);

    document.getElementById('lastUpdate-span').innerHTML = lastUpdate_msg

    var testEvolution = chartData[chartData.length - 1].evolution

    var signBefore = testEvolution === 0 ? "" : testEvolution > 0 ? " + " : " - ";
    var chartsTitle_msg = (moment(lastUpdate).isValid() && testEvolution > 0) ?
        'Evolution des soutiens<sup><small>**</small></sup> au ' + moment(lastUpdate).format('DD/MM/YYYY') +
        '<sup><small>(1)</small></sup><span class="badge badge-secondary ml-1"> + ' +
        chartData[chartData.length - 1].evolution.toLocaleString('fr') + '</span>' :
        "Pas d'évolution ou pas de nouvelle mise à jour... ";

    document.getElementById('charts-title').innerHTML = chartsTitle_msg;

    var contentDropdown = (isMobile) ? '' :
        `<select id="selectCharts" class="form-control form-control-sm" style="width: 200px">
        <option value="evolution">Voir l'&eacute;volution</option>
        <option value="tendance">Moyennes journalières</option>
        <option value="geoDpt">Nuage de points (dpts)</option>
        <option value="geoVille">Nuage de points (villes)</option>
        </select>`;
    // <option value="sparklines">Evolution par département</option>
    document.getElementById('tendance-dropdown').innerHTML = contentDropdown;

    if (!isMobile)
        document.getElementById('selectCharts').onchange = (ev) => {
            if (ev.target.value === "evolution") {
                if (typeof document.getElementById("chartdiv").data === 'undefined') { //initialize plot si abscent
                    drawChart(config)
                }

                document.getElementById('charts-title').innerHTML =
                    (moment(lastUpdate).isValid()) ? chartsTitle_msg : "Evolution des soutiens au ?"
                $('#chartTendance-div').hide()
                $('#date-btn').hide()
                $('#chartGeoVille-div').hide()
                $('#chartGeoDpt-div').hide()
                $('#tableChart-div').hide();
                $('#gauge-container').fadeIn()
                $('#chartdiv').fadeIn()
                // $('#projection-btn').fadeIn()
                $('#prev-moyenne').fadeIn()
                $('#prev-remove').fadeIn()
            } else if (ev.target.value === "tendance") {
                document.getElementById('charts-title').innerHTML = (moment(lastUpdate).isValid()) ?
                    "Moyennes journalières au " + moment(lastUpdate).format('DD/MM/YYYY') : "Moyennes journalières au ?"
                $('#prev-moyenne').hide()
                $('#prev-remove').hide()
                $('#chartdiv').hide()
                $('#projection-btn').hide()
                $('#date-btn').hide()
                $('#chartGeoVille-div').hide()
                $('#chartGeoDpt-div').hide()
                $('#tableChart-div').hide();
                $('#gauge-container').fadeIn()
                $('#chartTendance-div').show()
                document.getElementById('chartTendance-div').style.height = "70vh"
                chartTendance(config)
            } else if (ev.target.value === "geoVille") {
                document.getElementById('charts-title').innerHTML =
                    'Soutiens par nombre d\'électeurs (villes)';
                // 'Pourcentage de soutiens en fonction du nombre d\'électeurs (villes)'; // titre initial
                $('#chartTendance-div').hide();
                $('#prev-moyenne').hide();
                $('#prev-remove').hide();
                $('#chartdiv').hide();
                $('#projection-btn').hide();
                $('#tableChart-div').hide();
                $('#chartGeoDpt-div').hide();
                $('#gauge-container').fadeIn()
                $('#chartGeoVille-div').fadeIn();
                $('#date-btn').fadeIn()
                document.getElementById('chartGeoVille-div').style.height = "70vh";
                chartGeographique(true, config);
            } else if (ev.target.value === "geoDpt") {
                document.getElementById('charts-title').innerHTML =
                    'Soutiens par nombre d\'électeurs (départements)';
                // 'Pourcentage de soutiens en fonction du nombre d\'électeurs (départements)'; // titre initial
                $('#chartTendance-div').hide();
                $('#prev-moyenne').hide();
                $('#prev-remove').hide();
                $('#chartdiv').hide();
                $('#projection-btn').hide();
                $('#tableChart-div').hide();
                $('#chartGeoVille-div').hide();
                $('#gauge-container').fadeIn()
                $('#chartGeoDpt-div').fadeIn();
                $('#date-btn').fadeIn()
                document.getElementById('chartGeoDpt-div').style.height = "70vh";
                chartGeographique(false, config);
            } else if (ev.target.value === "sparklines") {
                document.getElementById('charts-title').innerHTML = "Evolution par département " +
                    '<small>avec progression du taux</small>'
                //+ moment(lastUpdate).format('LL') + '<sup><small> ***</small></sup>'
                $('#gauge-container').hide()
                $('#chartTendance-div').hide()
                $('#prev-moyenne').hide()
                $('#prev-remove').hide()
                $('#chartdiv').hide()
                $('#projection-btn').hide()
                $('#chartGeoVille-div').hide()
                $('#chartGeoDpt-div').hide()
                $('#date-btn').hide()
                $('#tableChart-div').fadeIn();
            }

            window.history.pushState('', '', '?g=' + $("select#selectCharts").prop('selectedIndex'));
        };

    // optionsChart pour plotly.js
    var optionsChart = {
        showSendToCloud: true,
        responsive: true,
        locale: 'fr'
    };

    document.getElementById('chartCSV-btn').onclick = function () {
        var dataExport = [];
        chartData.forEach(item => {
            dataExport.push([moment(item["date"]).format("LL"), item["visits"], item["cumul_theorique"],
                item["evolution"], item["difference"], item["moyenne_mobile"], item["moyenne_minimale"]
            ])
        })
        dataExport.unshift(["Date", "Soutiens publiés", "Soutiens nécessaires", "Evolution", "Différence", "Moyenne journalière", "Moyenne minimale"]);
        exportToCsv("données-graphiques.csv", dataExport);
    };

    var config = {
        chartData: chartData,
        latestCount: latestCount,
        minimumNeeded: cumul,
        dataSerie_officiel: dataSerie_officiel,
        days_complete: days_complete,
        days_done: days_done,
        days_remaining: days_remaining,
        contentStats: contentStats,
        optionsChart: optionsChart,
        lastUpdate: lastUpdate,
        lastMinimale: lastMinimale,
        lastMoyenne: lastMoyenne,
        average_last30Days: average_last30Days,
        average_last14Days: average_last14Days,
        testEvolution: testEvolution
    }

    return config;
}

function drawGauge(config) {
    $('#gauge-container').show()
    var counter = config.latestCount,
        needed = config.minimumNeeded,
        y = config.chartData.filter(item => item.evolution >= 0).map(item => item.evolution),
        x = config.chartData.filter(item => item.evolution >= 0).map((item, i) => i);

    // indicator soutiens publiés
    document.getElementById('publish_indic_title').innerHTML = "Soutiens publiés !"
    Plotly.newPlot(publish_indic, [{
            type: "indicator",
            mode: "number+delta",
            value: counter,
            number: {
                valueformat: ",.0f"
            }, // font: { color: '#67B7DC'}
            delta: {
                reference: needed,
                valueformat: ",.0f"
            }, //, increasing: { color: "#00FF00" }, decreasing: { color: "#FF6FA9" } },
        },
        {
            y: y,
            marker: {
                color: '#67B7DC'
            },
            hovertemplate: "<b>Soutiens publiés</b><br>" +
                "%{y:,.0f}<br>" +
                "<extra></extra>"
        }
    ], {
        height: 150,
        margin: {
            "t": 5,
            "b": 35,
            "l": 60,
            "r": 10,
            "pad": 0
        },
        xaxis: {
            title: "nombre jours",
            range: [0, d3.max(x) + 20]
        },
        font: {
            color: "white"
        },
        paper_bgcolor: "#353D42",
        plot_bgcolor: "#353D42",
        yaxis: {
            tickformat: (",.0f")
        }
    }, $.extend({}, config.optionsChart, {
        displayModeBar: false
    }))

    // indicator Moyenne sur les 14 derniers jours
    document.getElementById('moyenne_indic_title').innerHTML = 'Moyenne sur les 14 derniers jours <sup><small>*</small></sup>'
    var deltaMoyennes = (config.lastMinimale - config.lastMoyenne) + config.lastMinimale
    Plotly.newPlot(moyenne_indic, [{
        // domain: {x: [0, 1], y: [0, 1]},
        value: config.average_last14Days,
        type: "indicator",
        mode: "number", // "gauge+number+delta"
        number: {
            valueformat: ",.0f"
        },
        // delta: { reference: config.lastMinimale, valueformat: ",.0f", increasing: { color: "#00FF00" }, decreasing: { color: "#FF333F" } },
    }], {
        height: 50,
        margin: {
            "t": 0,
            "b": 0,
            "l": 10,
            "r": 10
        },
        font: {
            color: "white"
        },
        paper_bgcolor: "#353D42",
        plot_bgcolor: "white",
    }, $.extend({}, config.optionsChart, {
        displayModeBar: false
    }))

    // indicator moyenne minimale
    document.getElementById('minimal_indic_title').innerHTML = 'Moyenne minimale <sup><small>*</small></sup>'
    var deltaMoyennes = (config.lastMinimale - config.lastMoyenne) + config.lastMinimale
    Plotly.newPlot(minimal_indic, [{
        // domain: {x: [0, 1], y: [0, 1]},
        value: config.lastMinimale,
        type: "indicator",
        mode: "number", // "gauge+number+delta"
        number: {
            valueformat: ",.0f"
        },
        // delta: { reference: perDay_required, valueformat: ",.0f", increasing: { color:  "#FF333F"}, decreasing: { color: "#00FF00" } },
    }], {
        height: 50,
        margin: {
            "t": 0,
            "b": 0,
            "l": 10,
            "r": 10
        },
        font: {
            color: "white"
        },
        paper_bgcolor: "#353D42",
        plot_bgcolor: "white",
    }, $.extend({}, config.optionsChart, {
        displayModeBar: false
    })).then(function () {
        ;
    })

    // indicator moyenne journalière
    Plotly.newPlot("evolution-info", [{
        // domain: {x: [0, 1], y: [0, 1]},
        value: config.lastMoyenne,
        title: { text: "Moyenne depuis le 13/06/2019*" },
        type: "indicator",
        mode: "gauge+number", //+delta
        number: {
            valueformat: ",.0f"
        },
    }], {
        height: 300,
        margin: {
            "t": 70,
            "b": 25,
            "l": 25,
            "r": 25
        },
        // font: {
        //     color: "white"
        // },
        // paper_bgcolor: "#353D42",
        // plot_bgcolor: "white",
    }, $.extend({}, config.optionsChart, {
        displayModeBar: false
    }))

}

function drawChart(config) {
    var x = config.chartData.map((item, i) => i)

    var dataVisits = config.chartData.filter((item, i) => i > 4),
        date_max = config.chartData.map(item => item["date"])[config.chartData.length - 1],
        dataVisits_max = dataVisits.map(item => item["visits"])[dataVisits.length - 1],
        cumulTheorique_max = config.chartData.map(item => item["cumul_theorique"])[config.chartData.length - 1]


    var trace1 = {
        x: config.chartData.map(item => item["date"]),
        y: config.chartData.map(item => item["cumul_theorique"]),
        name: "Soutiens nécessaires",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#e59165',
            width: 3
        },
        visible: 'legendonly',
        text: config.chartData.map(item => item["cumul_theorique"]).map((item, i) => (i === config.chartData.length - 1) ? item.toLocaleString() + " " : ""),
        textposition: 'left',
        textfont: {
            family: 'sans serif',
            size: 18,
            color: '#e59165'
        },
        hovertemplate: "<b>soutiens nécessaires</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    }

    var trace2 = {
        x: dataVisits.map(item => item["date"]),
        y: dataVisits.map(item => item["visits"]),
        name: "Soutiens publiés",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#67B7DC',
            width: 3
        },
        text: dataVisits.map(item => item["visits"]).map((item, i) => (i === dataVisits.length - 1) ? item.toLocaleString() + " " : ""),
        textposition: 'left top',
        textfont: {
            family: 'sans serif',
            size: 18,
            color: '#67B7DC'
        },
        hovertemplate: "<b>soutiens publiés</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    }

    var trace3 = {
        x: config.dataSerie_officiel.map(item => item["date"]),
        y: config.dataSerie_officiel.map(item => item["comptage_off"]),
        name: "Comptage officiel (Conseil Constitutionnel)",
        type: 'scatter',
        mode: 'markers+text',
        marker: {
            color: '#808000',
            size: 10
        },
        textfont: {
            family: 'sans serif',
            size: 14,
            color: '#6C6C00'
        },
        text: config.dataSerie_officiel.map(item => item["comptage_off"].toLocaleString()),
        textposition: 'center bottom',
        hovertemplate: "<b>Comptage officiel</b><br>" +
            "%{text}<br>" +
            "<extra></extra>"
    }

    var diff = (cumulTheorique_max - dataVisits_max)
    var colorDiff = diff > 0 ? 'AB0055' : '#00AA00'
    var textDiff = diff > 0 ? 'manquants' : 'bonus'

    var trace4 = {
        x: [date_max, date_max],
        y: [dataVisits_max, cumulTheorique_max],
        ids: 'special',
        name: 'Soutiens ' + textDiff,
        type: 'scatter',
        mode: 'lines+text',
        visible: 'legendonly',
        line: {
            color: colorDiff,
            dash: 'dashdot',
            width: 1
        },
        hovertemplate: "<b>" + textDiff + "</b><br>" +
            diff.toLocaleString('fr') + "<br>" +
            "<extra></extra>"
    }

    var annotationsToMillion = [{
            showarrow: false,
            text: '<b>' + moment(config.dateToMillion).format("LL") + '*</b>',
            font: {
                size: 14,
                color: 'rgba(51, 51, 51, 0.7)'
            },
            xref: 'paper',
            yref: 'paper',
            x: 0.95,
            y: 0.45
        },
        {
            showarrow: false,
            text: "* date possible pour le million de soutiens (estimation)",
            font: {
                size: 12,
                color: 'rgba(51, 51, 51, 0.7)'
            },
            xref: 'paper',
            yref: 'paper',
            x: 0.5,
            y: 0.05
        }
    ];

    var annotationsManquants = [{
        // flèche manquants
        x: date_max,
        y: cumulTheorique_max - (diff / 2),
        xref: 'x',
        yref: 'y',
        text: diff.toLocaleString('fr') + '<br>' + textDiff,
        font: {
            size: 16,
            color: colorDiff
        },

        showarrow: true,
        arrowhead: 3,
        arrowcolor: colorDiff,
        ax: -70,
        ay: 0
    }];

    var annotations = [{
        showarrow: false,
        text: "Pour afficher ou masquer une courbe, cliquez sur une légende",
        font: {
            size: 12,
            color: colorDiff
        },
        xref: 'paper',
        yref: 'paper',
        x: 0.5,
        y: 0.05
    }];

    var layout = {
        font: {
            size: 16
        },
        margin: {
            l: 80,
            r: 25,
            b: 50,
            t: 35,
            pad: 4
        },
        yaxis: {
            automargin: true,
            tickformat: (",.0f"),
            tickfont: {
                color: 'rgb(38, 102, 148)',
                size: 14
            }
        },
        legend: {
            xanchor: "auto",
            yanchor: "top",
            orientation: "h",
            font: {
                size: 18
            }
        }
    }

    // création virtuelle array pour permettre l'animation entre 2 points
    var n = 12
    var diffArray = []
    var datesMax = []
    for (var i = 0; i < n; i++) {
        datesMax.push(date_max)
        diffArray.push(dataVisits_max)
        dataVisits_max += (cumulTheorique_max - dataVisits_max) / n
    }
    diffArray.splice(diffArray.length - 1, 1, cumulTheorique_max)

    var frames = []
    var x = datesMax
    var y = diffArray

    for (var i = 0; i < n; i++) {
        frames[i] = {
            data: [{
                x: [],
                y: []
            }]
        }
        frames[i].data[0].x = x.slice(0, i + 1)
        frames[i].data[0].y = y.slice(0, i + 1)
    }

    Plotly.newPlot(chartdiv, [trace4, trace1, trace2, trace3], layout, config.optionsChart)
        .then(function () {
            $('#chartdiv').fadeTo(500, 1);
            $("#chartdiv").find('g.legendpoints').find('text').css('visibility', 'hidden');
            document.getElementById('projection-select').value = "choice";
        })
        .then(function () {
            Plotly.animate(chartdiv, frames, {
                    transition: {
                        duration: 0
                    },
                    frame: {
                        duration: 100,
                        redraw: false
                    }
                })
                .then(function () {
                    Plotly.relayout(chartdiv, {
                            annotations: annotations
                        })
                        .then(function () {
                            // getDeptsTop(config)
                            $("#mainloader").hide();
                        })
                })
        })
        .then(function () {
            d3.select('g.legend').selectAll('.traces').each(function () {
                var item = d3.select(this);
                item.on('click', function (d) {
                    var trace = d[0].trace;
                    var name = trace.name
                    var vis = trace4.visible;
                    if (name === "Soutiens manquants" && vis !== true) {
                        Plotly.relayout(chartdiv, {
                            annotations: [...annotations, ...annotationsManquants]
                        })
                    } else if (name !== "Soutiens manquants" && vis === true) {
                        Plotly.relayout(chartdiv, {
                            annotations: [...annotations, ...annotationsManquants]
                        })
                    } else {
                        Plotly.relayout(chartdiv, {
                            annotations: annotations
                        })
                    };

                });
            });
        })

    function removeSeries() {
        document.getElementById('publish_indic_title').innerHTML = "Soutiens publiés <sup><small>*</small></sup> depuis " + config.days_done + " jours"
        document.getElementById('minimal_indic_title').innerHTML = 'Moyenne minimale <sup><small>*</small></sup'
        document.getElementById('moyenne_indic_title').innerHTML = 'Moyenne sur les 14 derniers jours <sup><small>*</small></sup>'
        Plotly.restyle(minimal_indic, {
            value: config.lastMinimale
        }, [0])
        Plotly.restyle(moyenne_indic, {
            value: config.average_last14Days,
            delta: {
                reference: config.lastMinimale,
                valueformat: ",.0f"
            }
        }, [0])
        Plotly.restyle(publish_indic, {
            value: config.latestCount,
            delta: {
                reference: config.minimumNeeded,
                valueformat: ",.0f"
            }
        }, [0])
        Plotly.restyle(publish_indic, {
            y: [config.chartData.map(item => item.evolution)]
        }, [1])
        // document.getElementById('minimal_indic').classList.remove("animated", "flash", "infinite")
        drawChart(config)
    }

    // $("#mainloader").hide()

    document.getElementById('projection-select').onchange = function () {
        (this.value !== "choice") ? addSeries(config): drawChart(config)
        document.getElementById('publish_indic_title').innerHTML = "Soutiens publiés <sup><small>*</small></sup> depuis " + config.days_done + " jours"
        document.getElementById('minimal_indic_title').innerHTML = 'Moyenne minimale <sup><small>*</small></sup>'
        document.getElementById('moyenne_indic_title').innerHTML = 'Moyenne sur les 14 derniers jours <sup><small>*</small></sup>'
        Plotly.restyle(minimal_indic, {
            value: config.lastMinimale
        }, [0])
        Plotly.restyle(moyenne_indic, {
            value: config.average_last14Days,
            delta: {
                reference: config.lastMinimale,
                valueformat: ",.0f"
            }
        }, [0])
        Plotly.restyle(publish_indic, {
            value: config.latestCount,
            delta: {
                reference: config.minimumNeeded,
                valueformat: ",.0f"
            }
        }, [0])
        Plotly.restyle(publish_indic, {
            y: [config.chartData.map(item => item.evolution)]
        }, [1])
    }
    document.getElementById('prev-remove').onclick = removeSeries
}


// ============ projections ============================ //
function addSeries(config) {
    $('#chartdiv').fadeTo("fast", 0.1)
    var selectedValue = document.getElementById('projection-select').value
    var select = document.getElementById('projection-select')
    var selectedText = select.options[select.selectedIndex].text

    var config = config
    var dataVisits = config.chartData.filter((item, i) => i > 4)

    var latestCount = config.latestCount,
        projection = config.latestCount,
        minimumNeeded = config.minimumNeeded,
        lastMoyenne = config.lastMoyenne, //config[selectedValue], ??
        lastMinimale = config.lastMinimale,
        days = 1,
        data_projection = []

    for (var i = 0; i < config.days_remaining + 1; i++) {
        var dateStr = moment(config.lastUpdate).add(i, 'days').format("YYYY-MM-DD")
        var obj = {}
        obj["date"] = dateStr
        obj["prevision_needed"] = minimumNeeded
        obj["prevision_moyenne"] = latestCount
        obj["prevision_minimale"] = projection
        data_projection.push(obj)
        minimumNeeded += perDay_required
        latestCount += config.average_last14Days
        projection += lastMinimale
    }

    var trace1 = {
        x: config.chartData.map(item => item["date"]),
        y: config.chartData.map(item => item["cumul_theorique"]),
        name: "Soutiens nécessaires",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#e59165',
            width: 5
        },
        text: config.chartData.map(item => item["cumul_theorique"]).map((item, i) => (i === config.chartData.length - 1) ? item.toLocaleString() + " " : ""),
        textposition: 'top',
        textfont: {
            family: 'sans serif',
            size: 18,
            color: '#e59165'
        },
        hovertemplate: "<b>Soutiens nécessaires</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    }

    var trace2 = {
        x: dataVisits.map(item => item["date"]),
        y: dataVisits.map(item => item["visits"]),
        name: "Soutiens publiés",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#67B7DC',
            width: 5
        },
        text: dataVisits.map(item => item["visits"]).map((item, i) => (i === dataVisits.length - 1) ? item.toLocaleString() + " " : ""),
        textposition: 'top',
        textfont: {
            family: 'sans serif',
            size: 18,
            color: '#67B7DC'
        },
        hovertemplate: "<b>Soutiens publiés</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    }

    var trace3 = {
        x: data_projection.map(item => item["date"]),
        y: data_projection.map(item => item["prevision_needed"]),
        name: "Projection soutiens nécessaires",
        fill: 'tozeroy',
        fillcolor: 'rgba(238, 153, 102, 0.05)',
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#e59165',
            dash: 'dot',
            width: 1
        },
        text: data_projection.map(item => item["prevision_needed"]).map((item, i) => (i === data_projection.length - 1) ? "4 717 396 " : ""),
        textposition: 'left',
        textfont: {
            family: 'sans serif',
            size: 16,
            color: '#e59165'
        },
        hovertemplate: "<b>Projection soutiens nécessaires</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    }

    var trace4 = {
        x: data_projection.map(item => item["date"]),
        y: data_projection.map(item => item["prevision_moyenne"]),
        name: "Prévision soutiens publiés",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#67B7DC',
            dash: 'dot',
            width: 4
        },
        text: data_projection.map(item => item["prevision_moyenne"])
            .map((item, i) => (i === data_projection.length - 1) ?
                Math.round(config.average_last14Days * config.days_remaining + config.latestCount).toLocaleString('fr') + " " : ""),
        textposition: 'left',
        textfont: {
            family: 'sans serif',
            size: 16,
            color: '#67B7DC'
        },
        hovertemplate: "<b>Prévision soutiens publiés</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    }

    var trace5 = {
        x: data_projection.map(item => item["date"]),
        y: data_projection.map(item => item["prevision_minimale"]),
        name: "Moyenne minimale à faire : " + Math.round(config.lastMinimale).toLocaleString('fr') + ' (soutiens/jour)',
        type: 'scatter',
        mode: 'lines',
        line: {
            color: '#AB0055',
            dash: 'dashdot',
            width: 2
        },
        hovertemplate: "<b>Prévision sur moyenne minimale</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    }

    var layout = {
        font: {
            size: 18
        },
        margin: {
            l: 85,
            r: 25,
            b: 50,
            t: 35,
            pad: 4
        },
        yaxis: {
            automargin: true,
            tickformat: (",.0f"),
            tickfont: {
                color: 'rgb(38, 102, 148)',
                size: 14
            },
        },
        legend: {
            xanchor: "auto",
            yanchor: "top",
            orientation: "h"
        },
        annotations: [{
                showarrow: false,
                text: "<b>PROJECTION</b>",
                font: {
                    size: 30,
                    color: 'rgba(51, 51, 51, 0.7)'
                },
                xref: 'paper',
                yref: 'paper',
                x: 0.2,
                y: 0.85
            },
            {
                showarrow: false,
                text: 'nombre de soutiens ' + selectedText,
                font: {
                    size: 16,
                    color: '#3D3D3D'
                },
                xref: 'paper',
                yref: 'paper',
                x: 0.2,
                y: 0.75
            },
            {
                showarrow: false,
                text: "Cliquez sur une légende pour afficher ou masquer une série",
                font: {
                    size: 16,
                    color: '#9E6245'
                },
                xref: 'paper',
                yref: 'paper',
                x: 0.9,
                y: 0.1
            }
        ]
    }

    var frames = []
    var x = data_projection.map(item => item["date"])
    var y = (selectedValue === "lastMoyenne") ?
        data_projection.map(item => item["prevision_moyenne"]) :
        data_projection.map(item => item["prevision_minimale"])

    var modulo = 16 // permet d'accélérer l'animation... voir autre méthode le cas échéant ?
    var n = data_projection.length + modulo
    for (var i = 0; i < n; i++) {
        if (i % modulo === 0) { // à revoir le cas échéant pour accélérer la projection
            frames[i] = {
                data: [{
                    x: [],
                    y: []
                }]
            }
            frames[i].data[0].x = x.slice(0, i + 1)
            frames[i].data[0].y = y.slice(0, i + 1)
        }
    }

    var dataFrames = (selectedValue === "lastMoyenne") ? [trace4, trace1] : [trace5, trace1]

    Plotly.newPlot('chartdiv', dataFrames, layout, config.optionsChart)
        .then(function () {
            Plotly.addTraces('chartdiv', trace2)
            // =================================== //
            animateGauge(config, y, selectedValue, selectedText, modulo)
        })
        .then(function () {
            Plotly.addTraces('chartdiv', trace3)
        })
        .then(function () {
            // Plotly.addTraces('chartdiv', trace4) // inutile dans cette config, pm
            $("#chartdiv").find('g.legendpoints').find('text').css('visibility', 'hidden')
        })
        .then(function () {
            $('#chartdiv').fadeTo(1000, 1)
            Plotly.animate('chartdiv', frames, {
                transition: {
                    duration: 0
                },
                frame: {
                    duration: 0,
                    redraw: false
                }
            })
        })

    chartdiv.on('*', function (ev) {
        console.log(ev)
    })
} // fin de addSeries


// utilisé dans generateConfig et updateGauge
function getMoyMinimale(moyenne_mobile, difference, days_remaining) {
    var moy = (moyenne_mobile >= perDay_required) ? perDay_required - (moyenne_mobile - perDay_required) :
        perDay_required + (Math.abs(difference) / days_remaining)
    return moy
}

function animateGauge(config, y, selectedValue, selectedText, modulo) {
    var requestAnimate
    var evolution = config.chartData.map(item => item.evolution)
    // console.log(evolution.length, y.length)
    for (var i = 0; i < y.length; i++) {
        var evolutionProjected = (i > 0) ? y[i] - y[i - 1] : null
        evolution.push(evolutionProjected)

    }
    // evolution.splice(config.chartData.map(item => item.evolution).length, 1) // à revoir car supprime 1 jour au final
    // console.log(evolution)

    var latestCount = config.latestCount
    var minimumNeeded = config.minimumNeeded
    var projectedMininmale = config.lastMinimale
    var projectedMoyenne = config.lastMoyenne
    var updateMinimale = {}
    var updateMoyenne = {}
    var updatePublish = {}
    var diffMinimale = (projectedMininmale - 17217) / y.length
    var diffMoyenne = (projectedMininmale - projectedMoyenne) / y.length

    var counter = 1
    var updateGauge = function () {
        if (projectedMininmale >= perDay_required) {
            projectedMininmale -= diffMinimale * modulo
            projectedMoyenne += diffMoyenne * modulo

            if (selectedValue !== "lastMoyenne") {
                updateMinimale["value"] = projectedMininmale >= perDay_required ? projectedMininmale : perDay_required
                updateMoyenne["value"] = projectedMoyenne <= perDay_required ? projectedMoyenne : perDay_required
                updateMoyenne["delta"] = {
                    reference: Math.round(updateMinimale["value"]),
                    valueformat: ",.0f"
                }

                latestCount += (config.lastMinimale * modulo)
                updatePublish["value"] = latestCount <= requiredTotal ? latestCount : requiredTotal
                updatePublish["delta"] = {
                    reference: requiredTotal,
                    valueformat: ",.0f"
                }

                Plotly.restyle(minimal_indic, updateMinimale, [0])
                Plotly.update(moyenne_indic, updateMoyenne, [0])

                Plotly.restyle(publish_indic, updatePublish, [0])
            } else {
                latestCount += (config.lastMoyenne * modulo)
                minimumNeeded += (perDay_required * modulo)
                updateMinimale["value"] = Math.round(getMoyMinimale(config.lastMoyenne, (latestCount - minimumNeeded), y.length))
                updateMoyenne["delta"] = {
                    reference: Math.round(updateMinimale["value"]),
                    valueformat: ",.0f"
                }

                updatePublish["value"] = latestCount <= y[y.length - 1] ? latestCount : y[y.length - 1]
                updatePublish["delta"] = {
                    reference: requiredTotal,
                    valueformat: ",.0f"
                }

                Plotly.restyle(minimal_indic, updateMinimale, [0])
                Plotly.update(moyenne_indic, updateMoyenne, [0])

                Plotly.restyle(publish_indic, updatePublish, [0])
            }
            counter++ // pour test
        } else {
            cancelAnimationFrame(requestAnimate)
            // clearInterval(interval)
            Plotly.restyle(publish_indic, {
                y: [evolution]
            }, [1])
            document.getElementById('publish_indic_title').innerHTML = 'Projection pour 274 jours <sup><small>*</small></sup> (' + selectedText + ')</small>'
            document.getElementById('minimal_indic_title').innerHTML = 'Moyenne minimale projetée au 12 mars 2020<sup><small>*</small></sup>'
            document.getElementById('moyenne_indic_title').innerHTML = 'Moyenne projetée au 12 mars 2020 <sup><small>*</small></sup><small>'
        }
    }

    // var interval = setInterval(updateGauge, 0)

    const animate = () => {
        requestAnimate = requestAnimationFrame(animate)
        updateGauge()
    }

    animate()
}

function chartTendance(config) {
    $('#chartTendance-div').fadeTo("fast", 0)

    var dataEvolution_x = config.chartData.filter(item => item["evolution"] >= 0).map(item => item["date"])
    var dataEvolution_y = config.chartData.map(item => item["evolution"])

    var trace1 = {
        x: dataEvolution_x,
        y: dataEvolution_y,
        name: "Soutiens publiés journaliers",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#67B7DC',
            // dash: 'dot',
            width: 3
        },
        text: dataEvolution_y.map((item, i) => (i === 0 || i === dataEvolution_y.length - 1) ? Math.round(item).toLocaleString('fr') : ""),
        textposition: 'top',
        textfont: {
            family: 'sans serif',
            size: 16,
            color: '#67B7DC'
        },
        hovertemplate: "<b>Soutiens publiés journaliers</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    }

    var trace2 = {
        x: config.chartData.map(obj => obj["date"]),
        y: config.chartData.map(obj => obj["moyenne_mobile"]),
        name: "Moyenne journalière",
        type: 'scatter',
        mode: 'lines+text',
        line: {
            color: '#0000FF',
            dash: "dot",
            width: 2
        },
        text: config.chartData.map(obj => obj["moyenne_mobile"]).map((item, i) => (i === dataEvolution_y.length - 1) ? Math.round(item).toLocaleString('fr') : ""),
        textposition: 'top',
        textfont: {
            family: 'sans serif',
            size: 16,
            color: '#0000FF'
        },
        hovertemplate: "<b>Moyenne journalière</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    }

    var trace3 = {
        x: config.chartData.map(obj => obj["date"]),
        y: config.chartData.map(obj => obj["moyenne_minimale"]),
        name: "Moyenne minimale à faire",
        type: 'scatter',
        mode: 'lines+text',
        visible: 'legendonly',
        line: {
            color: '#AB0055',
            dash: "dot",
            size: 2
        },
        text: config.chartData
            .map(obj => obj["moyenne_minimale"]).map((item, i) => (i === 0 || i === dataEvolution_y.length - 1) ? Math.round(item).toLocaleString('fr') : ""),
        textposition: 'top',
        textfont: {
            family: 'sans serif',
            size: 16,
            color: '#AB0055'
        },
        hovertemplate: "<b>Moyenne minimale à faire</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    }

    var trace4 = {
        x: config.chartData.map(obj => obj["date"]),
        y: config.chartData.map(obj => 17217),
        name: "Minimum théorique journalier",
        type: 'scatter',
        mode: 'lines+text',
        visible: 'legendonly',
        line: {
            color: '#E59165',
            dash: "dashdot",
            size: 3
        },
        text: config.chartData.map(obj => 17217).map((item, i) => (i === 0) ? Math.round(item).toLocaleString('fr') : ""),
        textposition: 'top',
        textfont: {
            family: 'sans serif',
            size: 16,
            color: '#E59165'
        },
        hovertemplate: "<b>Moyenne théorique journalière</b><br>" +
            "%{y:,.0f}<br>" +
            "<extra></extra>"
    }

    var data = [trace1, trace2, trace3, trace4]

    var layout = {
        font: {
            size: 18
        },
        margin: {
            l: 65,
            r: 20,
            b: 50,
            t: 35,
            pad: 4
        },
        yaxis: {
            automargin: true,
            tickformat: ("."),
            tickfont: {
                size: 14
            },
        },
        legend: {
            xanchor: "auto",
            yanchor: "top",
            orientation: "h"
        },
        annotations: [{
            showarrow: false,
            text: "Pour afficher ou masquer une courbe, cliquez sur une légende",
            font: {
                size: 12,
                color: '#9E6245'
            },
            xref: 'paper',
            yref: 'paper',
            x: 0.5,
            y: 0.95
        }]
    }

    var chart = Plotly.newPlot('chartTendance-div', data, layout, config.optionsChart).then(function () {
        $('#chartTendance-div').fadeTo(500, 1)
    })
    $("#chartTendance-div").find('g.legendpoints').find('text').css('visibility', 'hidden')
}

function getDataDepts(datesList, callback) {
    var dataTest = []
    datesList.forEach((date, i) => {
        d3.json(urlADPRipCarte + date + suffixADPRipDpt).then(function (data) {
            d3.json("./carto/chart-post-codes-departement.json").then(function (postCode) {
                date = moment.unix(+date).utc().startOf("days")
                for (var key in data) {
                    var obj = {}
                    var region = postCode[key] !== undefined ? postCode[key].region : "NA"
                    console.log()
                    obj["Date"] = date.format("YYYY-MM-DD")
                    obj["Région"] = region === "NA" ? "TAAF" : region
                    obj["Département"] = formatDepartements(data[key].nom)
                    obj["Code dept"] = key // (isNaN(+key)) ? key : +key
                    obj["Electeurs"] = data[key].electeurs
                    obj["Soutiens"] = data[key].soutiens
                    obj["Progression soutiens"] = 0
                    obj["Taux"] = (data[key].electeurs !== 0 && data[key].soutiens <= data[key].electeurs) ? (data[key].soutiens / data[key].electeurs) * 100 : 0
                    obj["Rang taux"] = ""
                    obj["Progression taux"] = 0
                    obj["Rang progression (Top)"] = ""
                    if (obj["Région"] !== "TAAF")
                        dataTest.push(obj)
                }
                callback(dataTest)
            })
        })
    })
}

function getProgressDepts(data) {
    var nestedData = d3.nest()
        .key(d => d["Département"])
        .rollup(v => {
            var dep = v.map(item => item["Département"])
            // console.log(dep)
            for (var i = 0; i < v.length; i++) {
                v[i]["Progression soutiens"] = (i > 0) ? v[i]["Soutiens"] - v[i - 1]["Soutiens"] : 0
                v[i]["Progression taux"] = (i > 0) ? v[i]["Taux"] - v[i - 1]["Taux"] : 0
            }
            return
        })
        .entries(data)
}

function getRank(data, testEvolution, dates) {
    d3.nest()
        .key(d => d["Date"])
        .rollup(v => {
            var top = v.map(item => item["Progression taux"]).sort((a, b) => b - a)
            var rank = v.map(item => item["Taux"]).sort((a, b) => b - a)
            var obj = {}
            for (var i = 0; i < v.length; i++) {
                if ((testEvolution >= 0 && v[i]["Date"] !== dates[0])) {
                    v[i]["Rang progression (Top)"] = 1 + top.indexOf(v[i]["Progression taux"])
                    v[i]["Rang taux"] = 1 + rank.indexOf(v[i]["Taux"])
                } else {
                    v[i]["Rang progression (Top)"] = ''
                    v[i]["Rang taux"] = ''
                }
            }
            return
        })
        .entries(data)
}

function getDeptsTop(config) {
    $.get(urlADPRipCarte, function (data) {
        datesList = parseDirListTS(data) //fillDataDateSelect(data, $('select#dataDate'))
        currentDataTs = datesList[0]
        var count = 0
        getDataDepts(datesList, function (data) {
            count++
            if (count === datesList.length) {
                // // voir pour fusionner les 2 communes en 1 département ?
                // data.forEach(d => {
                //     if (d["Département"] === "Saint Martin" || d["Département"] === "Saint Barthélemy")
                //         console.log(d)
                // })

                data = data.sort((a, b) => moment(a["Date"]).valueOf() - moment(b["Date"]).valueOf())
                var dates = [...new Set(data.map(item => item["Date"]))]

                getProgressDepts(data)
                getRank(data, config.testEvolution, dates)
                launchSparklines(data, config, dates)
            }
        })
    })
}

// ============ geographie ============================ //
var currentDataTs;
var currentChartGeo = "";
var scatterTraceCache = {
    "chartGeoVille-div": {},
    "chartGeoDpt-div": {}
};

function getScatterTrace(newTS, divID, callback) {
    if (divID in scatterTraceCache && newTS in scatterTraceCache[divID]) {
        callback(scatterTraceCache[divID][newTS])
        return
    }

    reloadDataTS(newTS, function (dpts, communes) {
        var ok_communes = Object.entries(communes).filter(c => c[1].electeurs > 0)
        scatterTraceCache["chartGeoVille-div"][newTS] = {
            x: ok_communes.map(c => c[1].electeurs),
            y: ok_communes.map(c => 100 * c[1].soutiens / c[1].electeurs),
            text: ok_communes.map(c => "<b>" + c[1].nom + " (" + c[0] + ")</b><br>Nombre de soutiens : " + c[1].soutiens.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + "<br>"),
            mode: 'markers',
            type: 'scattergl',
            marker: {
                size: 3
            },
            orientation: 'h',
            hovertemplate: "%{text}" + //nom commune et nombre soutiens
                "Nombre d'électeurs : %{x:,d}<br>" +
                "Taux : %{y:.2f}%" +
                "<extra></extra>",
            showlegend: false
        };

        var ok_dpts = Object.entries(dpts)
        scatterTraceCache["chartGeoDpt-div"][newTS] = {
            x: ok_dpts.map(c => c[1].electeurs),
            y: ok_dpts.map(c => 100 * c[1].soutiens / c[1].electeurs),
            text: ok_dpts.map(c => "<b>" + formatDepartements(c[1].nom) + " (" + c[0] + ")</b><br>Nombre de soutiens : " + c[1].soutiens.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + "<br>"),
            mode: 'markers',
            type: 'scattergl',
            marker: {
                size: 10
            },
            orientation: 'h',
            hovertemplate: "%{text}" + //nom dpt et nombre soutiens
                "Nombre d'électeurs : %{x:,d}<br>" +
                "Taux : %{y:.2f}%" +
                "<extra></extra>",
            showlegend: false
        };

        callback(scatterTraceCache[divID][newTS])
    })
}

function chartGeographique(isVille, config) { //charge les charts pour la repartition geographique (villes if isVille, or departements)
    if (isVille) {
        currentChartGeo = "chartGeoVille-div"
    } else {
        currentChartGeo = "chartGeoDpt-div"
    }

    if (typeof document.getElementById(currentChartGeo).data !== 'undefined') { //plot already plotted
        return
    }

    $("#mainloader").show();
    $('select#dataDate').prop('disabled', true);
    $.get(urlADPRipCarte, function (data) { //get a list of available data timestamp
        currentDataTs = fillDataDateSelect(data, $('select#dataDate'));
        getScatterTrace(currentDataTs, currentChartGeo, function (graphTrace) {
            var layout = {
                autosize: true,
                xaxis: {
                    type: 'log',
                    title: {
                        text: 'Nombre d\'électeurs',
                        font: {
                            family: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
                            size: 18,
                        }
                    }
                },
                yaxis: {
                    ticksuffix: ' %',
                    tickprefix: '  ',
                    title: {
                        text: 'Taux de soutiens',
                        font: {
                            family: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
                            size: 18,
                        }
                    }
                },
                shapes: [{
                    type: 'line',
                    xref: 'paper',
                    x0: 0,
                    y0: isVille ? 10 : 1,
                    x1: 1,
                    y1: isVille ? 10 : 1,
                    line: {
                        color: isVille ? 'rgb(50, 171, 96)' : 'rgb(255, 203, 48)',
                        width: 1,
                        dash: 'dashdot'
                    }
                }],
                // title: 'Pourcentage de soutiens en fonction du nombre d\'électeurs (' + (isVille ? 'villes' : 'départements') + ')',
                hovermode: 'closest'
            };

            if (isVille) {
                layout.shapes.push({
                    type: 'line',
                    xref: 'paper',
                    x0: 0,
                    y0: 5,
                    x1: 1,
                    y1: 5,
                    line: {
                        color: 'rgb(255, 203, 48)',
                        width: 1,
                        dash: 'dashdot'
                    }
                });
            }


            $("#mainloader").hide();
            $('select#dataDate').prop('disabled', false);
            Plotly.newPlot(currentChartGeo, [graphTrace], layout, config.optionsChart);

            /*
            //change la taille du point si hover - ne marche pas?
            var thisPlot = document.getElementById(currentChartGeo)
            thisPlot.on('plotly_hover', function(data) {
                var pn='',
                    tn='',
                    colors=[];
                for(var i=0; i < data.points.length; i++){
                    pn = data.points[i].pointNumber;
                    tn = data.points[i].curveNumber;
                    colors = data.points[i].data.marker.color;
                }
                colors[pn] = '#C54C82';
                var update = {'marker':{color: colors, size:2}};
                Plotly.restyle('chartGeoVille-div', update, [tn]);
            })

            thisPlot.on('plotly_unhover', function(data) {
                var pn='',
                    tn='',
                    colors=[];
                for(var i=0; i < data.points.length; i++){
                    pn = data.points[i].pointNumber;
                    tn = data.points[i].curveNumber;
                    colors = data.points[i].data.marker.color;
                }
                colors[pn] = '#00000';
                var update = {'marker':{color: colors, size:2}};
                Plotly.restyle('chartGeoVille-div', update, [tn]);
            })
            */
        })
    })
}
/* INTRO JS */
function setIntro() {
    document.getElementById('guide-btn').onclick = function (e) {

        var offsetHeigth = document.getElementById('chartTendance-div').offsetHeight;
        var intro = introJs();
        var chartTendance = {
            element: `#chartTendance-div`,
            intro: `<p>Cliquez sur les légendes du bas pour afficher ou masquer une série sur le graphique.</p>`,
            position: `top`
        };
        var options = [{
                intro: `<h4>Bienvenue dans la visite guidée.</h4><p>Vous pouvez interrompre la visite à tout moment
                        et y revenir quand vous voulez en cliquant sur "Guide"</p>`
            },
            {
                element: `#chartdiv`,
                intro: `<p>Cliquez sur les légendes du bas pour afficher ou masquer une série sur le graphique.</p>`,
                position: `top`
            },
            {
                element: `#gaugediv`,
                intro: ` <p>Nombre de soutiens publiés à ce jour, retard par rapport aux "soutiens nécessaires"
                et nombre de soutiens restant à valider pour atteindre l'objectif d’environ 4,7 millions de soutiens.</p>`,
                position: 'bottom'
            },
            {
                element: `#previsions-infos`,
                intro: `<p>Si la moyenne journalière des soutiens validés est trop basse, le "seuil minimum requis" d’environ 4,7 millions de soutiens,
                    soit 10% du corps électoral pourrait ne pas être atteint pour le 12/03/2019 date butoir.</p>
                    <p><a href="https://www.referendum.interieur.gouv.fr/" target="_blank" rel="noopener noreferrer" title="referendum.interieur.gouv.fr">En savoir plus...</a></p>
                    <p>La "moyenne minimale" à faire et à tenir pendant la durée des jours restants est actualisée journalièrement à partir de la moyenne
                    réellement effectuée et la "moyenne théorique" requise pour obtenir les 4 717 396 soutiens nécessaires.</p>
                    <p>Projections à partir de plusieurs moyennes du nombre final des soutiens qui pourrait être publiés à la date butoir du 12/03/2020
                    (visibles sur graphiques des moyennes journalières)</p>`,
                position: 'top'
            },
            {
                element: `#selectCharts`,
                intro: `Permet de passer d'un graphique à l'autre`,
                position: 'top'
            }
        ];
        if (offsetHeigth > 0) {
            options.splice(1, 1, chartTendance);
        }

        var optionsTendance =
            intro.setOptions({
                tooltipPosition: 'top',
                nextLabel: 'Suivant',
                prevLabel: 'Retour',
                skipLabel: 'Sortir',
                doneLabel: 'Terminer',
                showProgress: true,
                showStepNumbers: true,
                steps: options
            });
        intro.start();
    }
}

/* HELPERS */
function onStart() {
    if (isMobile) {
        $('#projection-btn').hide();
        $('#chartCSV-btn').hide();
        $('#tendance-infos').show();
        document.getElementById('application-btn').classList.add('mt-2');
        document.getElementById('stats-btn').innerHTML = '<span class="fa fa-stats mr-1" aria-hidden="true">';
    } else if (isTablet) {
        $('#chartCSV-btn').hide();
        // $('#projection-btn').show();
        document.getElementById('application-btn').classList.add('mt-2');
        // document.getElementById('prev-moyenne').innerHTML = 'Voir projection';
    } else {
        // $('#projection-btn').show();
        $('#chartCSV-btn').show();
    }

    $('#date-btn').hide();
}

function exportToCsv(filename, rows) {
    var processRow = function (row) {
        var finalVal = '';
        for (var j = 0; j < row.length; j++) {
            var innerValue = row[j] === null ? '' : row[j].toString();
            if (row[j] instanceof Date) {
                innerValue = row[j].toLocaleString();
            };
            var result = innerValue.replace(/"/g, '""');
            if (result.search(/("|,|\n)/g) >= 0)
                result = '"' + result + '"';
            if (j > 0)
                finalVal += ',';
            finalVal += result;
        }
        return finalVal + '\n';
    };

    var csvFile = '';
    for (var i = 0; i < rows.length; i++) {
        csvFile += processRow(rows[i]);
    }

    var blob = new Blob([csvFile], {
        type: 'text/csv;charset=utf-8;'
    });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}
