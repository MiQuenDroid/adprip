#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
The MIT License (MIT)

Copyright (c) 2017 Jieru Mei meijieru@gmail.com [https://github.com/meijieru/crnn.pytorch/]
Copyright (c) 2019 adprip.fr                    [https://gitlab.com/pierrenn/adprip]

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

import os
import sys
import subprocess
import string
import random
import requests
import copy

import shutil
import hashlib

from PIL import Image
from io import BytesIO


import torch
import torch.nn as nn
from torch.autograd import Variable
import torchvision.transforms as transforms

ALPHABET = string.digits+string.ascii_lowercase
TMP_FOLDER = os.path.join(os.path.expanduser("~"), "tmp/")

class BidirectionalLSTM(nn.Module):
    def __init__(self, nIn, nHidden, nOut):
        super(BidirectionalLSTM, self).__init__()

        self.rnn = nn.LSTM(nIn, nHidden, bidirectional=True)
        self.embedding = nn.Linear(nHidden * 2, nOut)

    def forward(self, input):
        recurrent, _ = self.rnn(input)
        T, b, h = recurrent.size()
        t_rec = recurrent.view(T * b, h)

        output = self.embedding(t_rec)  # [T * b, nOut]
        output = output.view(T, b, -1)

        return output


class CRNN(nn.Module):
    def __init__(self, imgH, nc, nclass, nh, n_rnn=2, leakyRelu=False):
        super(CRNN, self).__init__()
        assert imgH % 16 == 0, 'imgH has to be a multiple of 16'

        ks = [3, 3, 3, 3, 3, 3, 2]
        ps = [1, 1, 1, 1, 1, 1, 0]
        ss = [1, 1, 1, 1, 1, 1, 1]
        nm = [64, 128, 256, 256, 512, 512, 512]

        cnn = nn.Sequential()

        def convRelu(i, batchNormalization=False):
            nIn = nc if i == 0 else nm[i - 1]
            nOut = nm[i]
            cnn.add_module('conv{0}'.format(i),
                           nn.Conv2d(nIn, nOut, ks[i], ss[i], ps[i]))
            if batchNormalization:
                cnn.add_module('batchnorm{0}'.format(i), nn.BatchNorm2d(nOut))
            if leakyRelu:
                cnn.add_module('relu{0}'.format(i),
                               nn.LeakyReLU(0.2, inplace=True))
            else:
                cnn.add_module('relu{0}'.format(i), nn.ReLU(True))

        convRelu(0)
        cnn.add_module('pooling{0}'.format(0), nn.MaxPool2d(2, 2))  # 64x16x64
        convRelu(1)
        cnn.add_module('pooling{0}'.format(1), nn.MaxPool2d(2, 2))  # 128x8x32
        convRelu(2, True)
        convRelu(3)
        cnn.add_module('pooling{0}'.format(2),
                       nn.MaxPool2d((2, 2), (2, 1), (0, 1)))  # 256x4x16
        convRelu(4, True)
        convRelu(5)
        cnn.add_module('pooling{0}'.format(3),
                       nn.MaxPool2d((2, 2), (2, 1), (0, 1)))  # 512x2x16
        convRelu(6, True)  # 512x1x16

        self.cnn = cnn
        self.rnn = nn.Sequential(
            BidirectionalLSTM(512, nh, nh),
            BidirectionalLSTM(nh, nh, nclass))

    def forward(self, input):
        # conv features
        conv = self.cnn(input)
        b, c, h, w = conv.size()
        assert h == 1, "the height of conv must be 1"
        conv = conv.squeeze(2)
        conv = conv.permute(2, 0, 1)  # [w, b, c]

        # rnn features
        output = self.rnn(conv)

        return output

def model_dll(directory, url, md5sum):
    if not os.path.exists(directory):
        os.mkdir(directory)

    print("Downloading "+url+"...", file=sys.stderr)
    r = requests.get(url)
    content = BytesIO(r.content)
    md5 = hashlib.md5(content.getbuffer()).hexdigest()
    if md5 != md5sum:
        print("les md5sum ne correspondent pas! verifiez sur https://gitlab.com/pierrenn/adprip/wikis/home que le model est a jour", file=sys.stderr)
        sys.exit(-1)

    content.seek(0)
    with open(directory+'model.pth', 'wb') as f:
        shutil.copyfileobj(content, f)

CAPTCHA_MODEL = None
def load_captcha_model():
    ''' Resolution d'un captcha du site du RIP en utilisant PyTorch.
    '''
    global CAPTCHA_MODEL

    if CAPTCHA_MODEL == None:
        try:
            CAPTCHA_MODEL = CRNN(32, 1, 37, 256)
            model_path = "./captchas/model.pth"
            print('Loading pretrained model from "%s"...' % model_path)
            CAPTCHA_MODEL.load_state_dict(torch.load(model_path))
            CAPTCHA_MODEL.eval() #switch to evaluation mode
        except Exception as e:
            print("Error while loading model: "+str(e)+", trying to download a fresh one...", file=sys.stderr)
            model_dll("./captchas/", "https://gitlab.com/pierrenn/adprip/wikis/preprocess.pth", "a917d38362bbc9fffef206f6d91575bb")
            load_captcha_model()
            print("Model loaded. Going on...", file=sys.stderr)


TENSOR_TRANSFORM = transforms.ToTensor()
def run_neuralnet(fpath):
    '''Run neural net in CAPTCHA_MODEL on image in file fpath'''
    image = Image.open(fpath).convert('L')
    image = image.resize((100,32), Image.BILINEAR)
    image = TENSOR_TRANSFORM(image)
    image.sub_(0.5).div_(0.5)
    image = image.view(1, *image.size())
    image = Variable(image)

    preds = CAPTCHA_MODEL(image)
    _, preds = preds.max(2)
    preds = preds.transpose(1, 0).contiguous().view(-1)
    preds_size = Variable(torch.IntTensor([preds.size(0)]))

    if preds_size.data.numel() != 1:
        print("WARNING: preds_size = {}, using only the first one.".format(preds_size.data.numel()))

    length = preds_size.data[0]
    assert preds.data.numel() == length, "text with length: {} does not match declared length: {}".format(t.numel(), length)

    char_list = []
    for i in range(length):
        data = copy.deepcopy(preds.data)
        if data[i] != 0 and (not (i > 0 and data[i - 1] == data[i])):
            char_list.append(ALPHABET[data[i] - 1])

    return ''.join(char_list)

NEIB_DISTANCE = 2
STRIP_COLOR = [112]
OVERLAP_COLOR = 117
OVERLAP_COLOR_RGB = (OVERLAP_COLOR, OVERLAP_COLOR, OVERLAP_COLOR)
def vertical_neib(im, x, y, width, height, max = 10):
    '''Essaye d'enlever les traits clairement trop verticaux dans l'image'''

    found_down = 0
    found_up = 0
    found_down_rgb = None
    found_up_rgb = None
    # Look down
    for j in range(1, max):
        neib_y = y + j
        if neib_y > 0 and neib_y < height - 1:
            neib_rgb = im.getpixel((x, neib_y))
            if neib_rgb[0] not in STRIP_COLOR and neib_rgb[1] not in STRIP_COLOR and neib_rgb[2] not in STRIP_COLOR:
                if neib_rgb == OVERLAP_COLOR_RGB:
                    # Get next pixel
                    found_down = j + 1
                    found_down_rgb = im.getpixel((x, neib_y + 1))
                else:
                    found_down = j
                    found_down_rgb = neib_rgb
                break
    # Look up
    for j in range(1, max):
        neib_y = y - j
        if neib_y > 0 and neib_y < height:
            neib_rgb = im.getpixel((x, neib_y))
            if neib_rgb[0] not in STRIP_COLOR and neib_rgb[1] not in STRIP_COLOR and neib_rgb[2] not in STRIP_COLOR:
                if neib_rgb == OVERLAP_COLOR_RGB:
                    # Get next pixel
                    found_up = j + 1
                    found_up_rgb = im.getpixel((x, neib_y - 1))
                else:
                    found_up = j
                    found_up_rgb = neib_rgb
                break

    # Copy color of closest neighbor who is not STRIP_COLOR
    if found_down > 0 and found_down < found_up:
        return found_down_rgb
    elif found_up > 0:
        return found_up_rgb
    else:
        return (255, 255, 255)

def check_pixel(out, x, y, width, height):
    ''' Accept a pixel if it's not a letter color and not red (another path) '''
    if x >= 0 and x < width and y >= 0 and y < height:
        r, g, b = out.getpixel((x,y))
        if (r, g, b) != (140, 140, 140) and (r, g, b) != (255, 0, 0):
            return True
    return False

def find_path(out, in_x, in_y, width, height):
    ''' Find your way trough the letters '''
    path = []
    score = 0
    x = in_x
    y = in_y
    # Draw a split path
    while x >= 0 and x < width and y >= 0 and y < height - 1:
        r, g, b = out.getpixel((x,y))
        out.putpixel((x,y), (255, 0, 0))
        path.append((x,y))

        if check_pixel(out, x, y+1, width, height):
            y += 1 # Move down
            score += 1
        elif check_pixel(out, x, y+2, width, height):
            y += 2 # Move 2 down
            #out.putpixel((x,y-1), (255, 0, 0))
            path.append((x,y-1))
            score += 2
        elif check_pixel(out, x-1, y, width, height):
            x -= 1 # Move left
            score += 3
        elif check_pixel(out, x+1, y, width, height):
            x += 1 # Move right
            score += 3
        elif check_pixel(out, x-1, y+1, width, height):
            x -= 1 # Move down-left
            y += 1
            score += 3
        elif check_pixel(out, x+1, y+1, width, height):
            x += 1 # Move down-right
            y += 1
            score += 3
        elif check_pixel(out, x-2, y, width, height):
            x -= 2 # Move 2 left
            score += 4
        elif check_pixel(out, x+2, y, width, height):
            x += 2 # Move 2 right
            score += 4
        else:
            break
    if y < height - 1:
        score += 1000
    return (score, path)

MAX_FILTERED_PATH=20 #each image need to have the same width
def shining_elevator(filename, in_im, stretch):
    '''Fait couler des pixels rouge du haut en bas de l'image. Si stretch>0, stretch l'image en remplacant
       les pixels rouge qui arrivent jusqu'en bas de l'image par stretch pixels blanc.
    '''

    width, height = in_im.size
    tmp_img = Image.new('RGB', in_im.size, 0xFFFFFF)
    for x in range(width):
        for y in range(height):
            tmp_img.putpixel((x,y), in_im.getpixel((x,y)))

    y = 0
    scores = {}
    path_by_end_x = {0: [(0,y) for y in range(height-1)], 399: [(399,y) for y in range(height-1)]}
    for a in range(10, 390):
        x = 400 - a
        score, path = find_path(tmp_img, x, y, width, height)
        scores[a] = score
        if score < 500:
            path_by_end_x[path[len(path)-1][0]] = path

    if stretch == 0:
        return tmp_img

    all_end_x = [x for x in path_by_end_x.keys()]
    all_end_x.sort()
    filtered_end_x = []
    for i in range(len(all_end_x) - 1):
        if all_end_x[i+1] - all_end_x[i] > 5:
            filtered_end_x.append(all_end_x[i])
    filtered_end_x.append(all_end_x[len(all_end_x)-1])

    filtered_input_points = [] #list leftmost path point
    for end_x in filtered_end_x:
        this_path = []
        biggest_y = -1
        for (x_p, y_p) in path_by_end_x[end_x]:
            if y_p > biggest_y:
                if len(this_path)>0: #check extracted path in continuous in y
                    last_x, last_y = this_path[-1]
                    if last_y+1 != y_p:
                        print("WARNING: non continuous path for file ", filename)

                this_path.append((x_p,y_p))
                biggest_y = y_p

        #print(this_path)
        #print("---")
        filtered_input_points.append(this_path)

    ret_img = Image.new('RGB', (width+stretch*MAX_FILTERED_PATH, height), 0xFFFFFF)
    for y in range(height):
        out_x = 0
        filtered_idx = 0
        for x in range(width):
            if filtered_idx < MAX_FILTERED_PATH and filtered_idx < len(filtered_input_points) and (x,y) in filtered_input_points[filtered_idx]:
                filtered_idx += 1
                for _ in range(stretch):
                    ret_img.putpixel((out_x, y), (255,255,255))
                    out_x += 1
            else:
                old_px = in_im.getpixel((x,y))
                ret_img.putpixel((out_x, y), old_px)
                out_x += 1

    return ret_img

def blood_cleaning(img, blood_width):
    '''Remplace les pixels rouges dans l'image par des pixels blanc de width 2*blood_width+1'''
    width, height = img.size
    for x in range(width):
        for y in range(height):
            if img.getpixel((x,y)) == (255, 0, 0):
                img.putpixel((x,y), (255, 255, 255))
                if x>blood_width:
                    for i in range(1,blood_width+1):
                        img.putpixel((x-i,y), (255, 255, 255))

                if x<width-blood_width:
                    for i in range(1,blood_width+1):
                        img.putpixel((x+i,y), (255, 255, 255))


def captcha_preprocess(filename, im):
    '''Preprocess un captcha pour aider le neural net'''

    out = Image.new('RGB', im.size, 0xFFFFFF)
    width, height = im.size
    for x in range(width):
        for y in range(height):
            r, g, b = im.getpixel((x,y))
            if r in STRIP_COLOR and g in STRIP_COLOR and b in STRIP_COLOR:
                out.putpixel((x,y), vertical_neib(im, x, y, width, height))
            else:
                out.putpixel((x,y), (r, g, b))

    top_cleaned = shining_elevator(filename, out, 0)
    blood_cleaning(top_cleaned, 1)
    width, height = top_cleaned.size

    img_rev = Image.new('RGB', top_cleaned.size, 0xFFFFFF)
    for x in range(width):
        for y in range(height):
            img_rev.putpixel((x,height-y-1), top_cleaned.getpixel((x,y)))

    all_cleaned_rev = shining_elevator(filename, img_rev, 0)
    blood_cleaning(all_cleaned_rev, 1)
    width, height = all_cleaned_rev.size

    all_cleaned = Image.new('RGB', all_cleaned_rev.size, 0xFFFFFF)
    for x in range(width):
        for y in range(height):
            all_cleaned.putpixel((x,height-y-1), all_cleaned_rev.getpixel((x,y)))

    img_before_convert = shining_elevator(filename, all_cleaned, 1) #finally stretch image by 1
    img_before_convert.save('%s%s.png' % (TMP_FOLDER, filename))

    convert_cmd0 = "convert %s%s.png -matte \( +clone -fuzz 5%% -transparent \"#8c8c8c\" \) -compose DstOut -composite -background white -flatten -define connected-components:area-threshold=11 -define connected-components:mean-color=true -connected-components 4 -threshold 50%% -background black -flatten %s%s-convert0.png" % (TMP_FOLDER, filename, TMP_FOLDER, filename)
    convert_cmd1 = "convert %s%s-convert0.png -background white -flatten -blur 0x2 %s%s-final.png" % (TMP_FOLDER, filename, TMP_FOLDER, filename)

    subprocess.call(convert_cmd0, shell = True)
    subprocess.call(convert_cmd1, shell = True)

    os.remove("%s%s.png" % (TMP_FOLDER, filename))
    os.remove("%s%s-convert0.png" % (TMP_FOLDER, filename))

    return "%s%s-final.png" % (TMP_FOLDER, filename)

def get_captcha(image):
    '''Getter solve captcha function'''

    filename =''.join(random.choices(string.ascii_uppercase + string.digits, k = 15))
    preprocessed = captcha_preprocess(filename, Image.open(BytesIO(image.content)))
    guessed = run_neuralnet(preprocessed)

    os.remove(preprocessed)
    return guessed

if __name__ == "__main__":
    if len(sys.argv)<2:
        print("provide int as input")
        sys.exit(-1)

    load_captcha_model()
    print("model loaded")

    directory = '../decode/data'+sys.argv[1]+'/'
    files = [f for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]
    for f in files:
        filepath = os.path.join(directory, f)
        im = Image.open(filepath)
        guessed = run_neuralnet(captcha_preprocess(f, im))
        expected = f.replace('.png', '').lower()
        os.remove("%s%s-final.png" % (TMP_FOLDER, f))

        if expected != guessed:
            print("Error: {} - {}", expected, guessed)
