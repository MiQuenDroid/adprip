#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import numpy as np
import statistics
import matplotlib.pyplot
from matplotlib.ticker import AutoMinorLocator
from sklearn import datasets, linear_model
from sklearn.metrics import r2_score

import locale
from datetime import datetime, timedelta


locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

def read_input(fpath):
    '''Lit le fichier history.dat, convertit au format numpy, avec x le nombre de jours depuis le 13 Aout'''
    file_data = np.loadtxt(fpath, delimiter=";", usecols=(0,1))
    x = file_data[:,0]
    y = file_data[:,1]

    #verifie que l'input est bien separee d'un jour
    cut_idx = 0
    for i in range(0, len(x)):
        if cut_idx == 0 and x[i] >= 1565771297:
            cut_idx = i
        if cut_idx != 0 and i+1<len(x):
            if x[i]>x[i+1] or (x[i+1]-x[i])/3600 < 18:
                raise Exception("history file contains 2 or more non 24h (min. 18h) spaced dates, can't generate stats")

    return np.arange(len(x)-cut_idx), y[cut_idx:]

def generate_logx(x, a):
    ''' change input X d'un nombre en unix timestamp d'un nombre en jour avec une transformation log(a+x) '''
    if a<1: #so that we d'ont log 0
        return None

    return np.log(a+x)

def get_model_r2(x,y,A):
    '''Retourne le variance score pour un model lineaire avec parametre A pour l'equation y=B*ln(a+x)+C'''
    model = linear_model.LinearRegression()
    model.fit(generate_logx(x,A)[:,np.newaxis], y)
    yfit=model.predict(generate_logx(x, A)[:, np.newaxis])
    return model, r2_score(y, yfit)

def find_a_param(x,y):
    '''Trouve le parametre A optimum dans l'equation y=B*ln(A+x)+C'''
    max_r2 = 0
    for A in range(1, 500): #TODO: binomial etc etc...
        model, r2 = get_model_r2(x,y,A)
        if r2>max_r2:
            max_r2 = r2
        else:
            return model, A-1

def project_model(x,y):
    '''Estime le nombre total de soutiens au 12 Mars 2020 et retourne la plus grosse erreure'''
    model, A = find_a_param(x,y)
    error = y - model.predict(generate_logx(x, A)[:, np.newaxis])
    final_estim = model.predict(generate_logx(np.array([211]), A)[:, np.newaxis])
    return final_estim[0], max(error)


def compute_stats(count, data):
    '''Retourne [moyenne, max, min] comme string'''
    mean = statistics.mean(data)
    return ["{:,}".format(int(round(x))).replace(",", " ") for x in [mean, max(data), min(data)]]

def get_date_graph(date, isFull, isEven, t):
    '''Retourne les xticks label du graphe'''
    if isEven:
        if isFull:
            return date.strftime("%d/%m")
        else:
            return date.strftime("%d")
    else:
        return " "*(t//2)


def generate_stats(fpath):
    '''Genere un couple (tableau de stats, graphique des nvx soutien/jour format matplotlib)'''
    start_date = datetime(2019, 6, 13)
    end_date = datetime(2020, 3, 12)
    today = datetime.now()

    x, history = read_input(fpath)
    estimate, max_error = project_model(x,history)

    last_prog = [ history[i+1]-history[i] for i in range(0, len(history)-1)]
    stats = [compute_stats(history[len(history)-1], d) for d in [last_prog[len(last_prog)-31:len(last_prog)],last_prog[len(last_prog)-14:len(last_prog)], last_prog[len(last_prog)-7:len(last_prog)]]]

    # Generate a figure with matplotlib</font>
    my_dpi=96
    matplotlib.style.use('ggplot')
    fig, ax = matplotlib.pyplot.subplots(figsize=(1000/my_dpi, 390/my_dpi), dpi=my_dpi)
    x_dates = [datetime.now() - timedelta(days=30-x) for x in range(31)]
    y_val = last_prog[len(last_prog)-31:]
    ax.plot([get_date_graph(x, xIdx==0 or xIdx==len(x_dates)-1, xIdx%2==0, xIdx) for xIdx, x in enumerate(x_dates)], y_val, linewidth=5)
    ax.set_title(u'Evolution des nouveaux soutiens sur le dernier mois')
    ax.set_xlabel(u'Date')
    ax.set_ylabel(u'Nouveaux soutiens / jour')

    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.grid(which='minor', axis='y', ls=':')
    fig.tight_layout()

    return [
            ["Statistiques adprip.fr au {} ({} jours restants) :".format(today.strftime("%d/%m/%Y"), int((end_date-today).days))],
            [],
            [stats[2][0],"moyenne sur les 7 derniers jours"],
            [stats[1][0],"moyenne sur les 14 derniers jours"],
            [stats[0][0],"moyenne sur les 30 derniers jours"],
            ["{:,} ± {:,}".format(10000*int(round(estimate/10000)), 1000*int(round(max_error/1000))).replace(",", " "), "estimation* du nombre final de soutiens publiés au 12/03/2020"],
            []
        ], fig
